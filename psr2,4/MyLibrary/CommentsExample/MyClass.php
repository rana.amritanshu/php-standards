<?php
namespace MyLibrary\CommentsExample;

use OtherLibrary\OtherModule\OtherClass;

/*
| This comment will be ignored by the Documentation Parser
| Use section to show the intent of the code/snippet.
*/
/**
 * MyClass Descrption
 * 
 * @author - add this when making a public library (usually there things are maintained in versioning)
 * @link - to any documentation
 * @property mixed $name - use this for implicit properties (__get(), __set()), use '@var' otherwise.
 * @method mixed implicitMethod() - use this for implicit methods, declare docs block on each method otherwise.
 */
class MyClass extends MyOtherClass implements MyInterface
{

    /**
     * My var description
     *
     * @var [type]
     */
    public $var1;
    /**
     * @var String $compounded1 Description
     * @var integer $compounded2 Description
     */
    public $compounded1, $compounded2;

    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Description
     *
     * @param integer $param1
     * @return String
     */
    public static function myStaticMethod(int $param1): String
    {
        //code goes here
        return '';
    }

    /**
     * Description
     *
     * @param string $firstParam
     * @return array
     */
    final public function myFinalMethod(string $firstParam): array
    {
        //code goes here
        return [];
    }


    /**
     * Description
     *
     * @return \stdClass
     */
    private function checkConditions(): \stdClass
    {
        $true = true;
        $false = false;
        $null = null;
        return new \stdClass;
    }
}
