<?php
namespace MyLibrary\MyModule;

use OtherLibrary\OtherModule\OtherClass;

class MyClass extends MyOtherClass implements MyInterface
{
    public function __construct()
    {
        parent::__construct();
    }

    public static function myStaticMethod(int $param): string
    {
        //code goes here
        return '';
    }

    final public function myFinalMethod(string $param): array
    {
        //code goes here
        return [];
    }

    private function checkConditions(): \stdClass
    {
        $true = true;
        $false = false;
        $null = null;
        return new \stdClass;
    }
}
